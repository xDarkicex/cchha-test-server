module github.com/xDarkicex/cchha_new_server

go 1.14

require (
	github.com/go-chi/chi v4.0.4+incompatible
	github.com/go-chi/chi/v4 v4.0.0-rc1
	github.com/juju/errors v0.0.0-20190930114154-d42613fe1ab9 // indirect
	github.com/scorredoira/email v0.0.0-20191107070024-dc7b732c55da
	github.com/sirupsen/logrus v1.5.0 // indirect
)
